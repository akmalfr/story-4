from django import forms
from . import models

class CreateSchedule(forms.ModelForm):
    class Meta:
        model = models.Schedule
        fields = {'kategori','kegiatan','waktu','tempat'}
    # hari = forms.CharField(label='Hari', max_length=100, required=True)
    # tanggal = forms.DateField(required=True)
    # jam = forms.TimeField(required=True)
    # kegiatan = forms.CharField(max_length = 100, required=True)
    # tempat = forms.CharField(max_length = 100, required=True)
    # kategori = forms.CharField(max_length = 100, required=True)
        labels = {
            'kategori':'Kategori',
            'kegiatan' : 'Kegiatan',
            'waktu' : 'Waktu',
            'tempat' : 'Tempat',
        }

        widgets = {
            'kategori': forms.TextInput(
                attrs={
                    'class': 'form-control',
                },
            ),
            'kegiatan':forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Masukkan nama kegiatan anda',
                },
            ),
            'waktu': forms.DateTimeInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'yyyy/mm/dd HH:MM'
                },
            ),
            'tempat': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Masukkan tempat kegiatan anda',
                },
            ),
        }
        
from django.db import models
from datetime import datetime

# Create your models here.
class Schedule(models.Model):
    kategori = models.CharField(max_length = 25, null=True)
    kegiatan = models.CharField(max_length = 25, null=True)
    waktu = models.DateTimeField(null = True)
    tempat = models.CharField(max_length = 25, null=True)

    def __str__(self):
        return "{}. {}".fomat(self.id, self.activity)
from django.shortcuts import render
from django.http import HttpResponse
from .forms import CreateSchedule
from .models import Schedule
from . import forms

# Create your views here.
def index(request):
    form = forms.CreateSchedule(request.POST)
    if form.is_valid():
        form.save()
        return redirect('terima')

    context = {
        'form' : form
    }
    return render(request, 'form.html', context)

def terima(request):
    all = Schedule.objects.all()

    context = {
        'data':all
    }

    return render(request, 'schedule.html', context)

def delete(request, id):
    Schedule.objects.filter(id=id).delete()
    return redirect('terima')

# def index(request):   
#     createSchedule = CreateSchedule()
#     argument = {
#         'generated_html' : createSchedule
#     }
#     return render(request, 'form.html', argument)

# def terima(request):
#     x = request.POST.get('isian')
#     z = Schedule(hari = x)
#     z.save()
#     all = Schedule.objects.all()
#     argument = {
#         'data' : x,
#         'before' : all,
#     }
#     return render(request, 'schedule.html', argument)